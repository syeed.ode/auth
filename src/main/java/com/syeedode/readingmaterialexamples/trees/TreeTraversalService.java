package com.syeedode.readingmaterialexamples.trees;

import java.util.Objects;

/**
 * Cracking the Coding Interview
 * Chapter 4: Trees and Graphs
 * pg: 103
 */
public class TreeTraversalService {

    // Visit LEFT node, CURRENT node, then RIGHT node
    public void inOrderTraversal(NodeForTree node) {
        if(Objects.nonNull(node)) {
            // inOrderTraversal(node.left);

        }
    }

    public void visit(NodeForTree node) {

    }
}
