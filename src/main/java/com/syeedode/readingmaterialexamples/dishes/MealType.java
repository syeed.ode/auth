package com.syeedode.readingmaterialexamples.dishes;

public enum MealType {
      MEAT
    , FISH
    , OTHER
    , VEGETARIAN
}
